#!/bin/bash
#killall xiphos
rm heb_strongs.zip
rm -r source
rm *xml
wget http://www.walljm.com/misc/heb_strongs.zip
unzip heb_strongs.zip
sed -i '/^$/d' *.xml
sed -i '1,2d' *.xml
sed -i '$d' *.xml
mkdir source
rename 's/xml/txt/g' *xml
cat *txt >strongshebrew.xml
mv *txt source/

sed -r 's/<item id=\"H/<entryFree n=\"/g' strongshebrew.xml >strongshebrew.tei.xml
sed -ri 's/<\/item/<\/entryFree/g' strongshebrew.tei.xml
sed -ri 's/<\/title>/<\/orth><lb\/>/g' strongshebrew.tei.xml
sed -ri 's/<title/<orth/g' strongshebrew.tei.xml
sed -ri 's/\t\t<strong_id>.*$//g' strongshebrew.tei.xml
sed -ri '/^$/d' strongshebrew.tei.xml
sed -ri 's/<\/em>([).!:]*)<\/strong>/\1<\/em><\/strong>/g' strongshebrew.tei.xml

sed -ri 's/<strong>/<hi rend="bold">/g' strongshebrew.tei.xml
sed -ri 's/<\/strong>/<\/hi>/g' strongshebrew.tei.xml
sed -ri 's/<transliteration/<orth type=\"trans\" rend=\"bold\"/g' strongshebrew.tei.xml
sed -ri 's/<\/transliteration>/<\/orth>/g' strongshebrew.tei.xml
sed -ri ':a;N;$!ba;s/(<pronunciation>)\n\t\t\t(<em>)/\1\2/g' strongshebrew.tei.xml
sed -ri ':a;N;$!ba;s/(<\/em>)\n\t\t(<\/pronunciation>)/\1\2/g' strongshebrew.tei.xml
sed -ri 's/<pronunciation><em>/<pron rend=\"italic\">/g' strongshebrew.tei.xml
sed -ri 's/<\/em><\/pronunciation>/<\/pron><lb\/>/g' strongshebrew.tei.xml
sed -ri 's/description/def/g' strongshebrew.tei.xml
sed -ri 's/link target=\"H/ref target=\"StrongsHebrew:/g' strongshebrew.tei.xml
sed -ri 's/link/ref/g' strongshebrew.tei.xml
sed -ri 's/<strong><em>/<hi rend=\"italic\">/g' strongshebrew.tei.xml
sed -ri 's/<em>/<hi rend=\"italic\">/g' strongshebrew.tei.xml
sed -ri 's/<\/em><\/strong>|<\/em>|<\/strong>/<\/hi>/g' strongshebrew.tei.xml
sed -ri 's/<br\/>/<lb\/>/g' strongshebrew.tei.xml
sed -ri 's/<greek>|<\/greek>//g' strongshebrew.tei.xml

sed -ri 's/(<hi rend="bold">)<hi rend="italic">/\1/g' strongshebrew.tei.xml
sed -ri 's/(<\/hi>)<\/hi>/\1/g' strongshebrew.tei.xml
sed -ri 's/<pronunciation>/<pron rend="italic">/g' strongshebrew.tei.xml
sed -ri 's/<\/pronunciation>/<\/pron><lb\/>/g' strongshebrew.tei.xml
sed -ri 's/<sup>([a-z])<\/sup>/<hi rend="super">\1<\/hi>/g' strongshebrew.tei.xml
sed -ri 's/<sub>|<\/sub>//g' strongshebrew.tei.xml

### Traitement des noms de la bible
sed -ri 's/(bn=")1(")/\1Gen\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")2(")/\1Exod\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")3(")/\1Lev\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")4(")/\1Num\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")5(")/\1Deut\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")6(")/\1Josh\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")7(")/\1Judg\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")8(")/\1Ruth\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")9(")/\11Sam\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")10(")/\12Sam\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")11(")/\11Kgs\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")12(")/\12Kgs\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")13(")/\11Chr\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")14(")/\12Chr\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")15(")/\1Ezra\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")16(")/\1Neh\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")17(")/\1Esth\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")18(")/\1Job\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")19(")/\1Ps\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")20(")/\1Prov\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")21(")/\1Eccl\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")22(")/\1Song\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")23(")/\1Isa\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")24(")/\1Jer\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")25(")/\1Lam\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")26(")/\1Ezek\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")27(")/\1Dan\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")28(")/\1Hos\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")29(")/\1Joel\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")30(")/\1Amos\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")31(")/\1Obad\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")32(")/\1Jonah\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")33(")/\1Mic\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")34(")/\1Nah\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")35(")/\1Hab\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")36(")/\1Zeph\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")37(")/\1Hag\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")38(")/\1Zech\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")39(")/\1Mal\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")40(")/\1Matt\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")41(")/\1Mark\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")42(")/\1Luke\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")43(")/\1John\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")44(")/\1Acts\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")45(")/\1Rom\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")46(")/\11Cor\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")47(")/\12Cor\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")48(")/\1Gal\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")49(")/\1Eph\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")50(")/\1Phil\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")51(")/\1Col\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")52(")/\11Thess\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")53(")/\12Thess\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")54(")/\11Tim\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")55(")/\12Tim\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")56(")/\1Titus\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")57(")/\1Phlm\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")58(")/\1Heb\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")59(")/\1Jas\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")60(")/\11Pet\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")61(")/\12Pet\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")62(")/\11John\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")63(")/\12John\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")64(")/\13John\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")65(")/\1Jude\2/g' strongshebrew.tei.xml
sed -ri 's/(bn=")66(")/\1Rev\2/g' strongshebrew.tei.xml

sed -ri 's/<bib_ref bn="([0-9A-Za-z]*)" cn1="([0-9]*)" vn1="([0-9]*)"\/>/<ref osisRef="\1\.\2\.\3">\1 \2:\3<\/ref>/g' strongshebrew.tei.xml
sed -ri 's/<q>|<\/q>/\"/g' strongshebrew.tei.xml
cat header strongshebrew.tei.xml close >strongshebrewnv.xml
rm strongshebrew.tei.xml 
mv strongshebrewnv.xml strongshebrew.tei.xml
sed -i '36037d' strongshebrew.tei.xml
xmllint --noout --schema http://www.crosswire.org/OSIS/teiP5osis.2.5.0.xsd strongshebrew.tei.xml
mkdir /home/cyrille/.sword/modules/lexdict/zld/strongshebrew/
tei2mod /home/cyrille/.sword/modules/lexdict/zld/strongshebrew/ strongshebrew.tei.xml -z
cp strongshebrew.tei.xml ../
#xiphos &
